from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from portal.forms import *
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from .forms import UploadFileForm

@login_required
def portal_welcome(request):
    return render(request, 'portal/index.html', {'request': request})

def contact(request):
    #if the formhas been submitted

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']
            recipients = ['erry.robby@gmail.com']
            if cc_myself:
                recipients.append(sender)
            #send_mail(subject, message, sender, recipients)
            print('email mandata: ', subject, sender, recipients)
            #redirect after post

            return HttpResponseRedirect('/portal/thanks')

    else:
        #just visualize the form
        form = ContactForm()
        return render(request, 'portal/contact.html', {'form': form})

def thanks(request):
    return render(request, 'portal/thanks.html')

def upload(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            #handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('/portal/thanks')
    else:
        form = UploadFileForm()
        return render(request, 'uploadForm.html', {'form': form})

