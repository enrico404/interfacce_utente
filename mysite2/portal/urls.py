#controller dell'applicazione portal

from django.urls import path
from . import views

app_name = 'portal'

urlpatterns = [
    #ex /portal/
    path('', views.portal_welcome, name='portal_welcome'),
    path('contact/', views.contact, name='contact'),
    path('thanks/', views.thanks, name='thanks'),
    path('upload/', views.upload, name='upload')
]