from django.db import models
from django.utils import timezone
import datetime

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    #permessi aggiuntivi oltre ai default: add, delete and change
    class Meta:
        permissions = (('can_vote','Can Vote Polls'), )

    def was_published_recently(self):
        # timedelta serve in questo caso per togliere un giorno dal tempo attuale
        now = timezone.now()
        value = now - datetime.timedelta(days=1) <= self.pub_date <= now
        return value

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)