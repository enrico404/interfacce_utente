#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QUdpSocket>

class Manager : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(quint8 vehicleSpeed MEMBER m_vehicle_speed NOTIFY vehicleSpeedChanged)

    explicit Manager(QObject *parent = nullptr);

signals:

    void vehicleSpeedChanged();


public slots:    
    void onReceivedData();
    void processDatagram(QByteArray datagram);

private:
    QUdpSocket *socket;
   quint8 m_vehicle_speed;
};

#endif // MANAGER_H




