#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <Manager.h>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    Manager manager;


    QQmlApplicationEngine engine;


    const QUrl url(QStringLiteral("qrc:/main.qml"));


    engine.rootContext()->setContextProperty("manager",&manager);


    engine.load(url);


    return app.exec();
}












