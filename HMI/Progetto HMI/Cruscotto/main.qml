import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    visible: true
    width: 500
    height: 200
    title: qsTr("Cruscotto")


    Rectangle{
        id: background
        anchors.fill: parent
        color: "black"




        Image{
            id: leftArrow
            source: "qrc:/assets/right-arrow.png"
            width: 40
            height: 30
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: -30
            rotation: 180
        }


        Image{
            id: rightArrow
            source: "qrc:/assets/right-arrow.png"
            width: leftArrow.width
            height: leftArrow.height
            anchors.top: parent.top
            anchors.left: leftArrow.right
        }


        Rectangle{

            anchors.centerIn: parent
            width: parent.width/3
            height: width
            border.color: "white"
            radius: width/2
            color: "transparent"


        Text
		{

                id: speedText
                font.bold: true
                font.pixelSize: 50

                text: manager.vehicleSpeed

                color: "white"
                anchors.centerIn: parent

            }

        }





    }









}
