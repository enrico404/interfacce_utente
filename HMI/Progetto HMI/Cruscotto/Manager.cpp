#include "Manager.h"

Manager::Manager(QObject *parent) : QObject(parent)
{


    socket = new QUdpSocket(this);
    socket->bind(QHostAddress::LocalHost, 5000);


    connect(socket, SIGNAL(readyRead()), this, SLOT(onReceivedData()));
}



void Manager::onReceivedData()
{
    QByteArray datagram;

    datagram.resize(25);

    while(socket->hasPendingDatagrams())
    {
        socket->readDatagram(datagram.data(), datagram.length());
    }

    processDatagram(datagram);

}




void Manager::processDatagram(QByteArray datagram)
{

     m_vehicle_speed = datagram[15];

    
     emit vehicleSpeedChanged();

}
