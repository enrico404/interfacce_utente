#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QHostAddress>
#include <QTimer>
#include <QDebug>
#include <QDateTime>
#include <QDate>
#include <QTime>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void transmitMessage();
private:
    Ui::MainWindow *ui;
    QUdpSocket* comm_socket;
    quint16 comm_port;
    QByteArray msgData;    
    QTimer* msg_timer;

};

#endif // MAINWINDOW_H
