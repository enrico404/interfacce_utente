#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    comm_socket = new QUdpSocket(this);
    comm_port = 5000;

    msgData = QByteArray(25, 0x00);

    msg_timer = new QTimer(this);
    msg_timer->setInterval(1000);
    msg_timer->setSingleShot(false);

    connect(msg_timer, SIGNAL(timeout()), this, SLOT(transmitMessage()));

      msg_timer->start();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::transmitMessage()
{

    msgData[15] = static_cast<char>(ui->vehicle_speed_input->text().toInt());

    comm_socket->writeDatagram(msgData, QHostAddress::LocalHost,  5000);

}


